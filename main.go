// =========================================================================
// Copyright © 2022 Philip Marc Schwartz.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =========================================================================
package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"github.com/muesli/coral"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
)

const APP_NAME = "gitlab-authorized-keys-provider"
const ENDPOINT_FORMAT = "/users/%s/keys"

var (
	k        *koanf.Koanf
	logger   zerolog.Logger
	config   string
	logLevel string
)

func checkIfExpired(date *time.Time) bool {
	return time.Now().After(*date)
}

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	var rootCmd = &coral.Command{
		Use:   fmt.Sprintf("%s", APP_NAME),
		Short: "Retrieve Gitlab Public SSH Keys for a User",
		Long: `"gitlab-authorized-keys-provider" is an application used with OpenSSH AuthorizedKeysCommand
to retriever SSH Public Keys from users of a Gitlab sever.`,
		RunE: func(cmd *coral.Command, args []string) error {
			level, err := zerolog.ParseLevel(logLevel)
			if err != nil {
				level = zerolog.ErrorLevel
			}
			zerolog.SetGlobalLevel(level)

			k = koanf.New(".")
			configFile := file.Provider(config)
			if err := k.Load(configFile, yaml.Parser()); err != nil {
				log.Fatal().Err(err).Msgf("Failed to load config: %s", config)
			}

			logPath := fmt.Sprintf("%s/access-log", k.String("log_path"))
			logFile, err := os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
			if err != nil {
				log.Fatal().Err(err).Msgf("Failed to open log_path: %s", logPath)
			}

			logger = zerolog.New(logFile).With().Timestamp().Logger()

			url := k.String("gitlab_url")

			if len(args) != 1 {
				logger.Fatal().Msg("Missing username argument.")
			}

			user := args[0]

			userCtxLogger := logger.With().Str("User", user).Logger()
			userCtx := userCtxLogger.WithContext(context.Background())

			zerolog.Ctx(userCtx).Info().Msg("Creating Gitlab Client.")
			git, err := gitlab.NewClient("", gitlab.WithBaseURL(url))
			if err != nil {
				zerolog.Ctx(userCtx).Fatal().Err(err).Msg("Failed to create Gitlab Client")
			}

			zerolog.Ctx(userCtx).Info().Msg("Getting user keys from gitlab.")
			keys, _, err := git.Users.ListSSHKeysForUser(user, &gitlab.ListSSHKeysForUserOptions{})
			if err != nil {
				zerolog.Ctx(userCtx).Fatal().Err(err).Msgf("Failed to get user key for [%s]", user)
			}

			zerolog.Ctx(userCtx).Info().Msgf("%d keys found for user.", len(keys))
			for k, v := range keys {
				if v.ExpiresAt != nil {
					if checkIfExpired(v.ExpiresAt) {
						zerolog.Ctx(userCtx).Error().Msgf("Key %d expired at %s: %s", k, v.ExpiresAt.String(), v.Key)
						continue
					}
				}
				zerolog.Ctx(userCtx).Info().Msgf("Key %d is valid for login.: %s", k, v.Key)
				fmt.Printf("%s\n", v.Key)
			}
			return nil
		},
	}

	rootCmd.Flags().StringVarP(&config, "config", "c", fmt.Sprintf("/etc/%s/config.yaml", APP_NAME), "Specific Config file.")
	rootCmd.Flags().StringVarP(&logLevel, "level", "l", "info", "Specify the log level.")
	rootCmd.Execute()
}
